class Api {
  constructor(method, url, data, success, error) {
    this.method = method
    this.url = url
    this.data = data
    this.success = success
    this.error = error
  }
  call() {
    return $.ajax({
      method: this.method,
      url: this.url,
      data: this.data,
    })
    .done(this.success)
    .fail(this.error);
  }
}

$(function () {
  const API = `http://localhost:3000/tasks/`;
  function getAllTask() {
    let list = "";
    let data = new Object();
    let success = (res) => {
      res.map((value) => {
        list += `<li class="list-group-item pl-3 pr-0 row" id=${value.id}>
                    <input type="checkbox" 
                        class="form-check-input checkbox" 
                        ${value.status == 0 ? "" : "checked"}>
                    <span class="p-4 task col-11
                      ${value.status == 0 ? "" : "decorate"}
                      ">${value.task}</span>
                    <span class="float-right delete col-1"><i class="fa fa-times" aria-hidden="true"></i></span>
                  </li>`;
      });
      $("#list-task").html(list);
    }
    let error = () => {
      alert("Không thể hiển thị danh sách task");
    }
    let apiGetTask = new Api("GET", API, data, success, error);
    apiGetTask.call();
  }
  getAllTask();

  $("form#add-task").validate({
    rules: {
      "task": {
        required: true,
        maxlength: 50,
        minlength: 5,
      },
    },
    messages: {
      "task": {
        required: "Không được để trống công việc cần làm!",
        maxlength: "Hãy nhập tối đa 50 ký tự",
        minlength: "Hãy nhập tối thiểu 5 ký tự",
      },
    },
    submitHandler: function (element, e) {
      e.preventDefault();
      let $el = $(element);
      let data = $el.serialize();
      let success = () => {
        getAllTask();
        $el.find("#task-add").val("");
      }
      let error = () => {
        alert("Không thể thêm phần task vào danh sách!");
      }
      let apiPostTask = new Api("POST", API, data, success, error);
      apiPostTask.call();
    },
  })

  $("#list-task").on("click", ".task", function () {
    let task = $(this).text();
    let id = $(this).parent().attr("id");
    let status = $(this).prev().is(":checked") ? 1 : 0;
    $("form#update-task").addClass("d-block");
    $("form#update-task").attr("data-id", id);
    $("form#update-task").find("label.error").remove();
    $("input#task-update").val(task);
    $("input[name='status']").val(status);
    $("form#add-task").addClass("d-none");
    $("form#add-task").find("label.error").remove();
  })

  $("form#update-task").validate({
    rules: {
      "task": {
        required: true,
        maxlength: 50,
        minlength: 5
      },
    },
    messages: {
      "task": {
        required: "Không được để trống công việc cần làm!",
        maxlength: "Hãy nhập tối đa 50 ký tự",
        minlength: "Hãy nhập tối thiểu 5 ký tự"
      },
    },
    submitHandler: function (element, e) {
      e.preventDefault();
      let $el = $(element);
      let $parent = $el.parent();
      let $formAdd = $parent.find("#add-task");
      let id = $el.attr("data-id");
      let data = $el.serialize();
      let success = () => {
        getAllTask();
        $el.removeClass("d-block");
        $formAdd.removeClass("d-none");
        $formAdd.find("#task-add").val("");
      }
      let error = () => {
        alert("Không thể update task!");
      }
      let apiPutTask = new Api("PUT", API + id, data, success, error);
      apiPutTask.call();
    },
  })

  if ($("input.checkbox").is(":checked")) {
    $("input.checkbox").next().css({
      "text-decoration": "line-through",
      "text-decoration-color": "mediumblue",
    });
  } else {
    $("input.checkbox").next().css({ "text-decoration": "none" });
  }

  $("#list-task").on("click", "input.checkbox", function () {
    let id = $(this).parent().attr("id");
    let task = $(this).next().text();
    $(this).next().css({
      "text-decoration": "line-through",
      "text-decoration-color": "mediumblue",
    });
    let data = {
      'task': task,
      'status': $(this).is(":checked") ? 1 : 0
    };
    let success = () => {
      getAllTask();
    }
    let error = () => {
      alert("Không thể cập nhật trạng thái hoàn thành task này");
    }
    let apiPutStatus = new Api("PUT", API + id, data, success, error);
    apiPutStatus.call();
  });

  $("#list-task").on("click", ".delete", function () {
    let id = $(this).parent().attr("id");
    let data = new Object();
    let success = () => {
      $("form#add-task").removeClass("d-none");
      $("form#update-task").removeClass("d-block");
    }
    let error = () => {
      alert("Không thể xoá dữ liệu task này");
    }
    let apiDeleteTask = new Api("DELETE", API + id, data, success, error)
    apiDeleteTask.call();
    $(this).closest("li").remove();
  });
});
