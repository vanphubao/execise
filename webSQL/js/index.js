$(function() {
    var db = openDatabase('ToDoLIST', '1.0', 'Todo List', 5 * 1024 * 1024);
    db.transaction(function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS TASKS(task, status)");
    });
    $("form#add-task").on('submit', function() {
        let task = $(this).find("#task-add").val();
        db.transaction(function (tx) {
            tx.executeSql(`INSERT INTO TASKS (task, status) VALUES ('${task}', '0')`);
        })
        
    })  
    db.transaction(function (tx) {
        tx.executeSql("SELECT rowid, * FROM TASKS", [], (transaction, result) => {
            var len = result.rows.length;
            var list = '';
            for(let i = 0; i < len; i++) {
                list += `<li class="list-group-item pl-3 pr-0" id=${result.rows.item(i).rowid}>
                            <input type="checkbox" 
                                class="form-check-input checkbox" 
                                ${result.rows.item(i).status == 1 ?  'checked' : ''}>
                            <span class="p-4 task ${result.rows.item(i).status == 1 ? 'decorate' : ''}">${result.rows.item(i).task}</span>
                            <span class="float-right delete"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </li>`
            }
            
            $("#list-task").html(list);
        });

    });
    $("#list-task").on('click', '.delete', function() {
        let id = $(this).parent().attr('id');
        db.transaction(function (tx) {
            tx.executeSql(`DELETE FROM TASKS WHERE rowid='${id}'`);
        });
        $(this).closest("li").remove();
        location.reload();
    })

    $("#list-task").on('click', '.task', function() {
        let task = $(this).text();
        let id = $(this).parent().attr('id');
        $("form#update-task").addClass('d-block');
        $("form#add-task").addClass('d-none');
        $("input#task-update").val(task);
        $("input#id-update").val(id);
    })
    $("form#update-task").on('submit', function() {
        let taskUpdate = $(this).find("#task-update").val();
        let idUpdate = $(this).find("#id-update").val();
        db.transaction(function (tx) {
            tx.executeSql(`UPDATE TASKS SET task='${taskUpdate}' where rowid = '${idUpdate}'`);
        });
    })
    if ($('input.checkbox').is(":checked")) {
        $('input.checkbox').next().css({'text-decoration':'line-through', 'text-decoration-color':'mediumblue'});
    } else {
        $('input.checkbox').next().css({'text-decoration':'none'});
    }
    $("#list-task").on('click', 'input.checkbox', function() {
        let id = $(this).parent().attr('id');
        if ($(this).is(":checked")) {
            $(this).next().css({'text-decoration':'line-through', 'text-decoration-color':'mediumblue'});
            db.transaction(function (tx) {
                tx.executeSql(`UPDATE TASKS SET status='1' where rowid = '${id}'`);
            });
        } else {
            $(this).next().css({'text-decoration':'none'});
            db.transaction(function (tx) {
                tx.executeSql(`UPDATE TASKS SET status='0' where rowid = '${id}'`);
            });
        }
        location.reload();
    })
})
